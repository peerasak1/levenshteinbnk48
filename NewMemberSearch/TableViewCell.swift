//
//  TableViewCell.swift
//  NewMemberSearch
//
//  Created by Peerasak Unsakon on 30/7/2563 BE.
//  Copyright © 2563 Ookbee Co,. Ltd. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var thaiLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    var thai: String?
    var eng: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.thaiLabel.text = self.thai
        self.englishLabel.text = self.eng
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(th: String, en: String) {
        self.thai = th
        self.eng = en
    }

}
