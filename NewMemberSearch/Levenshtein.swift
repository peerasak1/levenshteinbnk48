import Foundation

extension String {
    
    var isThai: Bool {
        if let language = NSLinguisticTagger.dominantLanguage(for: self) {
            return language == "th"
        } else {
            return false
        }
    }
    
    subscript (i: Int) -> Character {
      return self[index(startIndex, offsetBy: i)]
    }
}

extension String {
    public func levenshtein(_ other: String) -> Int {
        // Check for empty strings first
        if (self.count == 0) {
            return other.count
        }
        if (other.count == 0) {
            return self.count
        }

        // Create an empty distance matrix with dimensions len(a)+1 x len(b)+1
        var dists = Array(repeating: Array(repeating: 0, count: other.count+1), count: self.count+1)

        // a's default distances are calculated by removing each character
        for i in 1...(self.count) {
            dists[i][0] = i
        }
        // b's default distances are calulated by adding each character
        for j in 1...(other.count) {
            dists[0][j] = j
        }

        // Find the remaining distances using previous distances
        for i in 1...(self.count) {
            for j in 1...(other.count) {
                // Calculate the substitution cost
                let cost = (self[i-1] == other[j-1]) ? 0 : 1

                dists[i][j] = Swift.min(
                    // Removing a character from a
                    dists[i-1][j] + 1,
                    // Adding a character to b
                    dists[i][j-1] + 1,
                    // Substituting a character from a to b
                    dists[i-1][j-1] + cost
                )
            }
        }

        return dists.last!.last!
    }
    
    public func editRatio(with text: String) -> Double {
        let minEditDistance = self.levenshtein(text)
        let length = self.count > text.count ? self.count : text.count
        return Double(minEditDistance)/Double(length)
    }
}
