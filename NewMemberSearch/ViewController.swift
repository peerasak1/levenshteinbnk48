//
//  ViewController.swift
//  NewMemberSearch
//
//  Created by Peerasak Unsakon on 30/7/2563 BE.
//  Copyright © 2563 Ookbee Co,. Ltd. All rights reserved.
//

import UIKit

let JSON = """
[
  {
    "en": "Cherprang",
    "th": "เฌอปราง"
  },
  {
    "en": "Faii",
    "th": "ฝ้าย"
  },
  {
    "en": "Jane",
    "th": "เจน"
  },
  {
    "en": "Jennis",
    "th": "เจนนิษฐ์"
  },
  {
    "en": "Jib",
    "th": "จิ๊บ"
  },
  {
    "en": "Kaew",
    "th": "แก้ว"
  },
  {
    "en": "Kate",
    "th": "เคท"
  },
  {
    "en": "Kheng",
    "th": "เข่ง"
  },
  {
    "en": "Korn",
    "th": "ก่อน"
  },
  {
    "en": "Minmin",
    "th": "มินมิน"
  },
  {
    "en": "Miori",
    "th": "มิโอริ"
  },
  {
    "en": "Myyu",
    "th": "มายยู"
  },
  {
    "en": "Niky",
    "th": "นิกี้"
  },
  {
    "en": "Noey",
    "th": "เนย"
  },
  {
    "en": "Panda",
    "th": "แพนด้า"
  },
  {
    "en": "Piam",
    "th": "เปี่ยม"
  },
  {
    "en": "Pun",
    "th": "ปัญ"
  },
  {
    "en": "Wee",
    "th": "วี"
  }
]

"""

struct Member: Codable {
    var th: String
    var en: String
}

class ViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var members: [Member] = []
    var results: [Member] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let jsonData = JSON.data(using: .utf8)!
        let members: [Member] = try! JSONDecoder().decode([Member].self, from: jsonData)
        self.members = members
        self.results = members
    }

    @IBAction func textFieldDidChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        self.search(text)
    }
    
    func search(_ text: String) {
        
        // หา ratio ความแตกต่าง
        let ratios = self.members.map { (member) -> (Member, Double) in
            if text.isThai {
                return (member, member.th.editRatio(with: text))
            }
            return (member, member.en.lowercased().editRatio(with: text.lowercased()))
        }
        
        // เรียงเอาที่แก้น้อยที่สุดขึ้นก่อน
        let sorted = ratios.sorted { (a, b) -> Bool in
            return  b.1 > a.1
        }
        
        // แปลงกลับเป็น member
        self.results = sorted.map({ (member, _) -> Member in
            return member
        })
        
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        cell.textLabel?.text = "\(self.results[indexPath.row].en.uppercased()) |  \(self.results[indexPath.row].th)"
        
        return cell
    }
    
    
}

extension ViewController: UITableViewDelegate {
    
}

